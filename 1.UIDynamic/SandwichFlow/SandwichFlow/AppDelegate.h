//
//  AppDelegate.h
//  SandwichFlow
//
//  Created by iLogiE MAC on 15-1-4.
//  Copyright (c) 2015年 iLogiE MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
- (NSArray *)sandwiches;

@end

