//
//  ViewController.m
//  SandwichFlow
//
//  Created by iLogiE MAC on 15-1-4.
//  Copyright (c) 2015年 iLogiE MAC. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewCell.h"
@interface ViewController ()<UICollectionViewDataSource>
{
    NSDictionary *_sandwich;
}

@property (nonatomic,weak) IBOutlet UIImageView *imageView;
@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic,weak) IBOutlet UITextView *textView;
@property (nonatomic,weak) IBOutlet UINavigationBar *navBar;
@end

@implementation ViewController

- (void)setSandwich:(NSDictionary *)sandwich
{
    _sandwich =sandwich;
    [self updateControlsWithYummySandwichData];
    
}
- (NSDictionary *)sandwich
{
    return _sandwich;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIImageView *backGroundImageView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Background-LowerLayer.png"]];
    backGroundImageView.bounds =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    backGroundImageView.alpha =0.5f;
    [self.view addSubview:backGroundImageView];
    [self.view sendSubviewToBack:backGroundImageView];
    
    [self.collectionView  registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.collectionView.dataSource =self;
    self.collectionView.backgroundColor =[UIColor clearColor];
    
    
    UICollectionViewFlowLayout *flowLayout =(UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    flowLayout.scrollDirection =UICollectionViewScrollDirectionHorizontal;
    flowLayout.itemSize =CGSizeMake(120, 20);
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSArray *ingredients =_sandwich[@"keywords"];
    return ingredients.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSArray *ingredients =_sandwich[@"keywords"];
    cell.title.text =ingredients[indexPath.row];
    
    return cell;
}
- (void)updateControlsWithYummySandwichData
{
    NSString *imageName =self.sandwich[@"image"];
    UIImage *image =[UIImage imageNamed:imageName];
    self.imageView.image =image;
    
    self.navBar.topItem.title =self.sandwich[@"title"];
    NSArray* instruction =self.sandwich[@"instructions"];
    self.textView.text =[instruction componentsJoinedByString:@"\n\n"];
}

@end
