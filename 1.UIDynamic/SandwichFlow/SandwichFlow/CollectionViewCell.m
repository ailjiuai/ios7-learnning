//
//  CollectionViewCell.m
//  SandwichFlow
//
//  Created by iLogiE MAC on 15-1-5.
//  Copyright (c) 2015年 iLogiE MAC. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell



- (id)initWithFrame:(CGRect)frame
{
    if (self ==[super initWithFrame:frame]) {
        
        _title =[[UILabel alloc]initWithFrame:CGRectInset(self.bounds, 3.0, 3.0)];
        _title.textAlignment =NSTextAlignmentCenter;
        _title.font =[UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:_title];
        
        self.layer.cornerRadius =5;
        self.layer.masksToBounds =YES;
        self.layer.backgroundColor =[UIColor colorWithWhite:0.8 alpha:1.0].CGColor;
        
        
    }
    return self;
}
@end
