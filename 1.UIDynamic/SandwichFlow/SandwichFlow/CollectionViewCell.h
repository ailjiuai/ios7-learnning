//
//  CollectionViewCell.h
//  SandwichFlow
//
//  Created by iLogiE MAC on 15-1-5.
//  Copyright (c) 2015年 iLogiE MAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (nonatomic,readonly) UILabel *title;
@end
