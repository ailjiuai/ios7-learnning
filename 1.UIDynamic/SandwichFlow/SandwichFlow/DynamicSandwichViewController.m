//
//  DynamicSandwichViewController.m
//  SandwichFlow
//
//  Created by iLogiE MAC on 15-1-4.
//  Copyright (c) 2015年 iLogiE MAC. All rights reserved.
//

#import "DynamicSandwichViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
@interface DynamicSandwichViewController ()<UICollisionBehaviorDelegate>
{
    NSMutableArray *_views;
    
    UIDynamicAnimator *_animator;
    UIGravityBehavior *_gravity;
    UISnapBehavior* _snap;
    BOOL _isDragging;
     BOOL _viewDocked;
    CGPoint _previousTouchPoint;
}
@end

@implementation DynamicSandwichViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImageView *backGroundImageView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Background-LowerLayer.png"]];
    backGroundImageView.frame =CGRectInset(self.view.frame, -50.0f, -50.0f);
    [self.view addSubview:backGroundImageView];
    [self addMotionEffectToView:backGroundImageView magnitude:50.0f];
    
//    // 2. add the background mid layer
    UIImageView* midLayerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background-MidLayer.png"]];
    
    [self.view addSubview:midLayerImageView];
    
    UIImageView * header =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Sarnie.png"]];
    header.center =CGPointMake(220, 190);
    [self.view addSubview:header];
    [self addMotionEffectToView:header magnitude:-20.0f];
    
    
    _animator =[[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    
    _gravity =[[UIGravityBehavior alloc]init];
    [_animator addBehavior:_gravity];
    
    _gravity.magnitude=0.4f;  //大小
    
    // add the view controllers
    _views = [NSMutableArray new];
    float offset = 250.0f;
    for (NSDictionary* sandwich in [self sandwiches]) {
        [_views addObject:[self addRecipeAtOffset:offset forSandwich:sandwich]];
        offset -= 50.0f;
    }
}
/**
 *  添加运动视觉效果
 *
 *  @param view
 *  @param magnitude
 */

- (void)addMotionEffectToView:(UIView*)view magnitude:(CGFloat)magnitude {
    UIInterpolatingMotionEffect* xMotion = [[UIInterpolatingMotionEffect alloc]
                                            initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    xMotion.minimumRelativeValue = @(-magnitude);
    xMotion.maximumRelativeValue = @(magnitude);
    UIInterpolatingMotionEffect* yMotion = [[UIInterpolatingMotionEffect alloc]
                                            initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    yMotion.minimumRelativeValue = @(-magnitude);
    yMotion.maximumRelativeValue = @(magnitude);
    UIMotionEffectGroup* group = [[UIMotionEffectGroup alloc] init];
    group.motionEffects = @[xMotion, yMotion];
    [view addMotionEffect:group];
}
- (NSArray *)sandwiches
{
    AppDelegate *appdelegate =[[UIApplication sharedApplication]delegate ];
    return appdelegate.sandwiches;
}
- (UIView *)addRecipeAtOffset:(CGFloat)offset forSandwich:(NSDictionary *)sandwich
{
    CGRect frameForView =CGRectOffset(self.view.bounds, 0.0, self.view.bounds.size.height-offset);
    
    UIStoryboard *myStoryboard =[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *viewController =[myStoryboard instantiateViewControllerWithIdentifier:@"VC"];
    
    UIView *view =viewController.view;
    view.frame =frameForView;
    viewController.sandwich =sandwich;
    
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
    [viewController didMoveToParentViewController:self];
    
    
    UIPanGestureRecognizer *pan =[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
    [viewController.view addGestureRecognizer:pan];
    UICollisionBehavior *collision =[[UICollisionBehavior alloc]initWithItems:@[view]];
    collision.collisionDelegate =self;
    [_animator addBehavior:collision];
    
    
    //最低的
    float bounary =view.frame.origin.y +view.frame.size.height+1;
    CGPoint bounaryStart =CGPointMake(0.0, bounary);
    CGPoint bounaryEnd =CGPointMake(self.view.frame.size.width, bounary);
    [collision addBoundaryWithIdentifier:@1 fromPoint:bounaryStart toPoint:bounaryEnd];
    
    
    //添加高的
    bounaryStart =CGPointMake(0.0, 0.0);
    bounaryEnd =CGPointMake(self.view.frame.size.width, 0.0);
    
    [collision addBoundaryWithIdentifier:@2 fromPoint:bounaryStart toPoint:bounaryEnd];
    
    
    [_gravity addItem:view];
    
    UIDynamicItemBehavior *dynamicItemBehavior =[[UIDynamicItemBehavior alloc]initWithItems:@[view]];
    [_animator addBehavior:dynamicItemBehavior];

    
    return view;
}
#pragma mark UICollisionBehaviorDelegate
- (void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id<UIDynamicItem>)item withBoundaryIdentifier:(id<NSCopying>)identifier atPoint:(CGPoint)p
{
    
    UIView *view = (UIView *)item;

    if ([@2 isEqual:identifier])
    {
        [self tryDockView:view];
    }
    
    if ([@1  isEqual:identifier])
    {
        UIDynamicItemBehavior *itemBehavior =[self itemBehaviourForView:view];
        CGPoint linearVelocityForCollision =[itemBehavior linearVelocityForItem:view];
    
        
        //将这个速度转给其他的view
        for (UIView *otherView in _views) {
            if (view !=otherView )
            {
                UIDynamicItemBehavior *itemBehavior =[self itemBehaviourForView:otherView];
                float bounceMagnitude =arc4random() %50 +linearVelocityForCollision.y*0.5 ;
                [itemBehavior addLinearVelocity:CGPointMake(0, bounceMagnitude) forItem:otherView];
            }
        }
    }
    
    
}
- (void)handlePan:(UIPanGestureRecognizer *)gesture
{
    CGPoint touchPoint =[gesture locationInView:self.view];
    UIView *draggedView =gesture.view;
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        UIView *draggedView =gesture.view;
        CGPoint  dragStartLocation =[gesture locationInView:draggedView];
        if (dragStartLocation.y <200.0f)
        {
            _isDragging= YES;
            _previousTouchPoint =touchPoint;  //开始的时候记录下自己的位置
        };
        
          }else if (gesture.state ==UIGestureRecognizerStateChanged)
    {
        if (_isDragging) {
            float yOffset =_previousTouchPoint.y -touchPoint.y;
            gesture.view.center =CGPointMake(draggedView.center.x, draggedView.center.y -yOffset);
            _previousTouchPoint=touchPoint;

        }
        
    }else if (gesture.state == UIGestureRecognizerStateEnded  && _isDragging )
    {
        _isDragging =NO;
        [self tryDockView:draggedView];
        
        [self addVelocityToView:draggedView fromGesture:gesture];
    }
    
}
- (UIDynamicItemBehavior *)itemBehaviourForView:(UIView*)view {
    for (UIDynamicItemBehavior *behaviour  in _animator.behaviors) {
        
        if (behaviour.class ==[UIDynamicItemBehavior class] && [behaviour.items firstObject ] ==view) {
            return behaviour;
        }
    }
    return nil;
}
- (void)addVelocityToView:(UIView*)view fromGesture:(UIPanGestureRecognizer*)gesture {
    CGPoint vel = [gesture velocityInView:self.view];

    vel.x = 0;
    UIDynamicItemBehavior* behaviour =
    [self itemBehaviourForView:view];
    [behaviour addLinearVelocity:vel forItem:view];  //添加一个线性速率，相当于可以返回自己的状态
}
- (void)tryDockView:(UIView *)view {
    
    BOOL viewHasReachedDockLocation = view.frame.origin.y < 100.0;
    if (viewHasReachedDockLocation) {
        if (!_viewDocked) {
            _snap = [[UISnapBehavior alloc] initWithItem:view snapToPoint:self.view.center];
            [_animator addBehavior:_snap];
            [self setAlphaWhenViewDocked:view alpha:0.0];
            _viewDocked = YES;
        }
    } else {
        if (_viewDocked) {

            [_animator removeBehavior:_snap];
            [self setAlphaWhenViewDocked:view alpha:1.0];
            _viewDocked = NO;
        }
    }
}
- (void)setAlphaWhenViewDocked:(UIView*)view alpha:(CGFloat)alpha {

    for (UIView* aView in _views) {
        
        if (aView != view) {
            aView.alpha = alpha;
        }
    }
}

@end
