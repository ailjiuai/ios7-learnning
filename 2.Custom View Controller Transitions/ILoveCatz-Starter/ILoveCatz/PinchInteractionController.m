//
//  PinchInteractionController.m
//
//
//  Created by iLogiE MAC on 15-1-6.
//  Copyright (c) 2015年 com.razeware. All rights reserved.
//

#import "PinchInteractionController.h"

@implementation PinchInteractionController
{
    UIViewController *_viewController;
    BOOL _shouldCompleteTransition;
    
    CGFloat _startScale;
}

- (void)wireToViewController:(UIViewController *)viewController
{
    _viewController =viewController;
    [self prepareGestureRecongizerInView:viewController.view];
}
- (void)prepareGestureRecongizerInView:(UIView *)view
{
    UIPinchGestureRecognizer *pinchGesture =[[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinchGesture:)];
    [view addGestureRecognizer:pinchGesture];
}
- (CGFloat)completionSpeed
{
    NSLog(@"self.percentComplete---%f",self.percentComplete);
    return 1-self.percentComplete;
}
- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gesture
{
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            _startScale =gesture.scale;
            self.interactionInProgress =YES;
            [_viewController dismissViewControllerAnimated:YES completion:nil];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            //获取得到已经缩放的大小的比列
            CGFloat fraction =1.0-gesture.scale/_startScale;
            
            _shouldCompleteTransition =(fraction >0.5);
            NSLog(@"fraction----%f",fraction);
            [self updateInteractiveTransition:fraction];
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            self.interactionInProgress =NO;
            if (!_shouldCompleteTransition || gesture.state ==UIGestureRecognizerStateCancelled) {
                [self cancelInteractiveTransition];
            }else
            {
                [self finishInteractiveTransition];
            }
        }
            break;
            
        default:
            break;
    }
}


@end
