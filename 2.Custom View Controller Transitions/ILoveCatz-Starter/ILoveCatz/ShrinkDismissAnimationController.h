//
//  ShrinkDismissAnimationController.h
//  ILoveCatz
//
//  Created by iLogiE MAC on 15-1-6.
//  Copyright (c) 2015年 com.razeware. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *  缩小dismiss动画视图
 */
@interface ShrinkDismissAnimationController : NSObject<UIViewControllerAnimatedTransitioning>

@end
