//
//  BouncePresentAnimationController.m
//  ILoveCatz
//
//  Created by iLogiE MAC on 15-1-5.
//  Copyright (c) 2015年 com.razeware. All rights reserved.
//

#import "BouncePresentAnimationController.h"

@implementation BouncePresentAnimationController

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.5;
}
// This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{

    NSLog(@"isAnimated--%d,isInteractive---%d",[transitionContext isAnimated],[transitionContext isInteractive]);
    //1.从上下文中获取状态
    UIViewController *fromViewControlleer =[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
//    fromViewControlleer.view.alpha =0.5;
     NSLog(@"fromViewController---%@",fromViewControlleer);
    UIViewController *toViewController =[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    NSLog(@"toViewController---%@",toViewController);
    CGRect finalFrame =[transitionContext finalFrameForViewController:toViewController];

   
    //2.获取包含的view
    UIView *containerView =[transitionContext containerView];
    
    //3 初始化状态
    CGRect screenBounds =[[UIScreen mainScreen] bounds];
    toViewController.view.frame =CGRectOffset(finalFrame, 0, screenBounds.size.height);
    
    //4 添加一个view
    [containerView addSubview:toViewController.view];
    //5.执行动画
    NSTimeInterval duration =[self transitionDuration:transitionContext];
    
//    [UIView animateWithDuration:duration animations:^{
//        toViewController.view.frame =finalFrame;
//    } completion:^(BOOL finished) {
//        fromViewControlleer.view.alpha =1.0f;
//        [transitionContext completeTransition:YES];
//    }];
    
    /**
     :returns: 震荡比列， 初始化刚开始的速度 spring动画，弹性动画
     */
    [UIView animateWithDuration:duration delay:0.0 usingSpringWithDamping:0.7 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        fromViewControlleer.view.alpha =0.5;
        toViewController.view.frame =finalFrame;
    } completion:^(BOOL finished) {
        fromViewControlleer.view.alpha =1.0f;
        [transitionContext completeTransition:YES];
    }];
}

@end
