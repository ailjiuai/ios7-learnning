//
//  BouncePresentAnimationController.h
//  ILoveCatz
//
//  Created by iLogiE MAC on 15-1-5.
//  Copyright (c) 2015年 com.razeware. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  弹性动画弹出视图
 */
@interface BouncePresentAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

@end
