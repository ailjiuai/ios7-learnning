//
//  DetailViewController.m
//  ILoveCatz
//
//  Created by Colin Eberhardt on 22/08/2013.
//  Copyright (c) 2013 com.razeware. All rights reserved.
//

#import "DetailViewController.h"
#import "Cat.h"


#define isSupport (kCFCoreFoundationVersionNumber > kCFCoreFoundationVersionNumber_iOS_7_1)
@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *attributionText;

@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageView.image = [UIImage imageNamed:self.cat.image];
    self.attributionText.text = self.cat.attribution;
    self.title = self.cat.title;

//    if (isSupport) {
//        self.navigationController.hidesBarsOnTap=YES;
//    }
   
    self.edgesForExtendedLayout =UIRectEdgeNone;
}

@end
