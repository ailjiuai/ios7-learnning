//
//  FlipAnimationController.h
//  ILoveCatz
//
//  Created by iLogiE MAC on 15-1-6.
//  Copyright (c) 2015年 com.razeware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlipAnimationController : NSObject<UIViewControllerAnimatedTransitioning>

@property (nonatomic,assign)BOOL reverse;
@end
