//
//  PinchInteractionController.h
//  ILoveCatz
//
//  Created by iLogiE MAC on 15-1-6.
//  Copyright (c) 2015年 com.razeware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PinchInteractionController : UIPercentDrivenInteractiveTransition

- (void)wireToViewController:(UIViewController *)viewController;
@property (nonatomic,assign)BOOL  interactionInProgress;
@end
