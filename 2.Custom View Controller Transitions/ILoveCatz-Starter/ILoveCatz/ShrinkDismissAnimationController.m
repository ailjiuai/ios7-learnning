//
//  ShrinkDismissAnimationController.m
//  ILoveCatz
//
//  Created by iLogiE MAC on 15-1-6.
//  Copyright (c) 2015年 com.razeware. All rights reserved.
//

#import "ShrinkDismissAnimationController.h"

@implementation ShrinkDismissAnimationController

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 2;
}
// This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    //获取上下文视图
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
   
    UIViewController *toViewController =[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];

    CGRect finalFrame =[transitionContext finalFrameForViewController:toViewController];
    NSLog(@"finalFrame---%@",NSStringFromCGRect(finalFrame));
    UIView *containerView =[transitionContext containerView];
    
    toViewController.view.frame =finalFrame;
    toViewController.view.alpha =0.5;
    
    [containerView addSubview:toViewController.view];
    [containerView sendSubviewToBack:toViewController.view];
    
    
    CGRect screenBounds =[[UIScreen mainScreen] bounds];
    CGRect shrunkenFrame =CGRectInset(fromViewController.view.frame, fromViewController.view.frame.size.width/4, fromViewController.view.frame.size.height/4);
    CGRect fromFinalFrame =CGRectOffset(shrunkenFrame, 0, screenBounds.size.height);
    
    NSTimeInterval duration =[self transitionDuration:transitionContext];
    
    /**
     *  获取fromViewController视图的快照, - (UIView *)snapshotViewAfterScreenUpdates:(BOOL)afterUpdates 中的afterUpdates代表是否将视图的所有效果都添加上在执行快照,为YES的话，表示是，如果snapshotViewAfterScreenUpdates后边设置view的alpha ＝0,则什么都不显示。如果为NO的话，则表示在当前的视图上快照
     */
    
//    UIView *intermediateView =[fromViewController.view snapshotViewAfterScreenUpdates:NO];
//    intermediateView.frame =fromViewController.view.frame;
//    [containerView addSubview:intermediateView];
//    
//    [fromViewController.view removeFromSuperview];

    [UIView animateKeyframesWithDuration:duration delay:0.0 options:UIViewKeyframeAnimationOptionCalculationModeCubic animations:^{
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.5 animations:^{
//            intermediateView.frame =shrunkenFrame;
             fromViewController.view.transform = CGAffineTransformMakeScale(0.5, 0.5);
            toViewController.view.alpha =0.5;
        }];
        
        [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:0.5 animations:^{
            fromViewController.view.frame =fromFinalFrame;
            toViewController.view.alpha =1.0f;
        }];
    } completion:^(BOOL finished){
//        [intermediateView removeFromSuperview];
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
     }];
    
    
    
}

@end
