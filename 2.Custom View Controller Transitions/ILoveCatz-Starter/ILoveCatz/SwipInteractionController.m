//
//  SwipInteractionController.m
//  ILoveCatz
//
//  Created by iLogiE MAC on 15-1-6.
//  Copyright (c) 2015年 com.razeware. All rights reserved.
//

#import "SwipInteractionController.h"
@implementation SwipInteractionController
{
    BOOL _shouldCompleteTransition;
    UINavigationController * _navigationController;
}
- (void)wireToViewController:(UIViewController *)viewController
{
    _navigationController =viewController.navigationController;
    [self prepareGestureRecognizerInView:viewController.view];
}
- (void)prepareGestureRecognizerInView:(UIView*)view {
    
    UIPanGestureRecognizer *gesture =[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    
    [view addGestureRecognizer:gesture];
    
}
- (void)handleGesture:(UIPanGestureRecognizer *)gesture
{
    /**
     *  坐标系的过度坐标点，当开始手势的时候，如果x 为正数变大则向右，如果x 为负数变小 则为向左移动
     */
    CGPoint translation =[gesture translationInView:gesture.view.superview];
//    NSLog(@"translation.x    %f",translation.x);
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
        {
          
            self.interactionInProgress =YES;
            [_navigationController popViewControllerAnimated:YES];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            //计算出显示过度是怎样完成的部分值
            CGFloat fraction = -(translation.x /200.0f);
        
            fraction =fminf((fmaxf(fraction, 0.0)), 1.0);
            
            _shouldCompleteTransition =(fraction >0.5 );
            
            [self updateInteractiveTransition:fraction];
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
           
            self.interactionInProgress =NO;
            if (!_shouldCompleteTransition ||gesture.state ==UIGestureRecognizerStateCancelled)
            {
                [self cancelInteractiveTransition];
            }else
            {
                [self finishInteractiveTransition];
            }
        }
            break;
            
        default:
            break;
    }
}

- (CGFloat)completionSpeed
{
    return 1-self.percentComplete;
}
@end
