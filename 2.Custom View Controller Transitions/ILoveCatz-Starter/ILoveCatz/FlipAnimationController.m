//
//  FlipAnimationController.m
//  ILoveCatz
//
//  Created by iLogiE MAC on 15-1-6.
//  Copyright (c) 2015年 com.razeware. All rights reserved.
//

#import "FlipAnimationController.h"
@implementation FlipAnimationController


- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 3.0;
}
// This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    UIView *containView =[transitionContext containerView];
    
    UIViewController *fromVC =[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC =[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *toView =toVC.view;
    UIView *fromView =fromVC.view;
    
    [containView addSubview:toView];
    

    CATransform3D transform =CATransform3DIdentity;
    transform.m34 =-0.002;
    [containView.layer setSublayerTransform:transform];
    
    CGRect initialFrame =[transitionContext initialFrameForViewController:fromVC];
    fromView.frame =initialFrame;
    toView.frame =initialFrame;
    
    float factor =self.reverse ? 1.0 :-1.0f;
    toView.layer.transform =[self yRotation:factor* -M_PI_2];
    
    NSTimeInterval duration =[self transitionDuration:transitionContext];
    
    [UIView animateKeyframesWithDuration:duration delay:0.0 options:UIViewKeyframeAnimationOptionCalculationModeCubic animations:^{
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.5 animations:^{
            
            fromView.layer.transform =[self yRotation:factor *M_PI_2];
        }];
        
        [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:0.5 animations:^{
            toView.layer.transform =[self yRotation:0.0];
        }];
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}
- (CATransform3D) yRotation:(CGFloat) angle {
    return  CATransform3DMakeRotation(angle, 0.0, 1.0, 0.0);
}
@end
